<div id="summit-wrap">

    <div id="summit-page">
    
    	<div id="summit-head-wrap">
        
        	<div id="summit-head-gradient">
    
                <header>
                
                    <a href="/summit/welcome-first-annual-leadership-newark-summit">
                    	<div id="logo">Leadership Newark Public Policy Summit 2013: Rebuilding the Dream that is Newark</div>
                    </a>
                                    
                </header>
            
            </div><!-- end #summit-head-gradient -->
            
            <div id="summit-nav-wrap">
                    
                <nav>
                <?php print render($page['summit-navbar']); ?>
                </nav>
            
            </div><!-- end #summit-nav-wrap -->
            
        </div><!-- end #summit-head-wrap -->
    
        <div id="summit-main">
        	
            <div id="summit-content">
            
            	<div id="summit-content-left">
                	
                    <!-- Leading image above title -->
                	<?php if ($page['lead-image']): print render($page['lead-image']); endif; ?>
                
                	<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
                 	<?php print render($page['content']); ?>
                
                </div><!-- end #summit-content-left -->
                
                <div id="summit-content-right">
                
                	<?php print render($page['summit-right']); ?>
                    
                </div><!-- end #summit-content-right -->
                
                <div class="myclear"></div>
                
            </div><!-- End #summit-content -->
        
        
        	<footer>
                <div id="summit-foot-left">
                	LEADERSHIP NEWARK<br/>
                    494 Broad Street, Suite LL10, Newark, NJ 01702<br/>
                    Phone: 973-350-9200&nbsp;|&nbsp;Fax: 973-350-9208<br/>
                    Email: <a href="mailto:information@leadershipnewark.org">information@leadershipnewark.org</a>
                </div>
				<div id="summit-foot-right">
                	&copy;2000 - 2013, Leadership Newark - Public Policy Summit. All rights reserved.<br/>Site by <a href="http://www.inspirawebdesign.com" target="_blank">Inspira Web Design.</a>
                </div>
            </footer>
        
        </div><!-- End #summit-main -->
    
    </div><!-- End #summit-page -->
    
</div><!-- End #summit-wrap -->