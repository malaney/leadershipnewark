/* Slideshow params */

jQuery(function(){
	jQuery("#slides").slides({
        preload: true,
        preloadImage: '/sites/all/themes/leadnewark/img/slides/loading.gif',
        generateNextPrev: true,
		generatePagination: false,
        pause: 2500,
		play: 8000,
        hoverPause: true
      });
});
