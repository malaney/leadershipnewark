<?php

// leadnewark functions 

// set cookie
setcookie('visited','1',86400);

// add body classes from node_class
function bodyclass() {
	
	if( (arg(0)=='node') && (is_numeric(arg(1))) ){ 

		$mynode = node_load(arg(1));
		if(isset($mynode->field_css_classes['und'][0]['value'])){
			$myclasses = $mynode->field_css_classes['und'][0]['value'];
			print $myclasses; 
		}
		
	}
}


// random image
function random_pic($dir)
{
    $files = glob($dir . '/*.*');
    $file = array_rand($files);
    return $files[$file];
}


// Customize the user login/register/password page titles
function leadnewark_preprocess_page(&$vars) {
  if (arg(0) == 'user') {
    switch (arg(1)) {
      case 'register':
        drupal_set_title(t('Register with Leadership Newark'));
        break;
      case 'password':
        drupal_set_title(t('Reset Your Password'));
        break;
      case '':
      case 'login':
        drupal_set_title(t('User login'));
        break;
    }
  }
}


// Print out Contextual links
function nwk_edit() {
	global $user;
	if (user_access('access_node_edit_front_end')) {
		$mynid = arg(1);
    	print '<div class="nwk-edit"><a href="/node/'.$mynid.'/edit">Edit this item</a></div>';
  }
}


// byline for stories - links author name to profile
function nwk_byline($myuid){

	// first check for associated profile
	$myprofileid = db_query("SELECT field_associated_profile_value FROM field_revision_field_associated_profile WHERE entity_type = 'user' AND entity_id = ".$myuid."")->fetchField();
	
	// if profile exists, get it
	if($myprofileid){
	$myprofilename = db_query("SELECT title FROM node WHERE nid = ".$myprofileid."")->fetchField();
	$mylink = "node/".$myprofileid;
	print '<div class="nwk-byline">by '.l($myprofilename, $mylink).'</div>';	
	} else {
		
	// if no profile id
	$myprofilename = db_query("SELECT name FROM users WHERE uid = ".$myuid."")->fetchField();
	print '<div class="nwk-byline">by '.$myprofilename.'</div>';		
	}
	
	


}




