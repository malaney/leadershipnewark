<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 */
?>
<div class="profile"<?php print $attributes; ?>>
  <?php hide($user_profile); ?>
  
  <!-- name and address -->
  <h2>
  <?php print $user_profile['field_first_name']['#items'][0]['value'].' '.$user_profile['field_last_name']['#items'][0]['value']; ?></h2>
  <p><strong>Personal Address:</strong><br/>
  <?php if(isset($user_profile['field_street_address'])){
	print $user_profile['field_street_address']['#items'][0]['value'].'<br/>';  
  }?>
  <?php if(isset($user_profile['field_city'])){
	print $user_profile['field_city']['#items'][0]['value'].', ';  
  }?>
  <?php if(isset($user_profile['field_state'])){
	print $user_profile['field_state']['#items'][0]['value'].' ';  
  }?>
  <?php if(isset($user_profile['field_zip_code'])){
	  	$mylen = strlen($user_profile['field_zip_code']['#items'][0]['value']);
	  		if($mylen == 4){print '0';} // fix shortened ones from Excel import
		print $user_profile['field_zip_code']['#items'][0]['value'];  
  }?>
  </p>
  
  <p style="margin-top:20px"><strong>Business Address:</strong><br/>
  <?php if(isset($user_profile['field_business_address'])){
	print $user_profile['field_business_address']['#items'][0]['value'].'<br/>';  
  }?>
  <?php if(isset($user_profile['field_business_city'])){
	print $user_profile['field_business_city']['#items'][0]['value'].', ';  
  }?>
  <?php if(isset($user_profile['field_business_state'])){
	print $user_profile['field_business_state']['#items'][0]['value'].' ';  
  }?>
  <?php if(isset($user_profile['field_business_zip'])){
	  	$mylen = strlen($user_profile['field_business_zip']['#items'][0]['value']);
	  		if($mylen == 4){print '0';} // fix shortened ones from Excel import
		print $user_profile['field_business_zip']['#items'][0]['value'];  
  }?>
  </p>
  
  <!-- phone numbers -->
  <p style="margin-top:20px">
  <?php if(isset($user_profile['field_cell_phone'])){
	print '<strong>Cell: </strong>';  
	print $user_profile['field_cell_phone']['#items'][0]['value'].'<br/>';  
  }?>
  <?php if(isset($user_profile['field_work_phone'])){
	print '<strong>Work: </strong>';  
	print $user_profile['field_work_phone']['#items'][0]['value'].'<br/>';  
  }?>
  <?php if(isset($user_profile['field_home_phone'])){
	print '<strong>Home: </strong>';  
	print $user_profile['field_home_phone']['#items'][0]['value'].'<br/>';  
  }?>
  <?php if(isset($user_profile['field_fax'])){
	print '<strong>Fax: </strong>';  
	print $user_profile['field_fax']['#items'][0]['value'].'<br/>';  
  }?>
  </p>
  
  <!-- emails -->
  <p style="margin-top:20px">
  <?php if(isset($user_profile['field_business_email'])){
	print '<strong>Work Email: </strong>';  
	print '<a href="mailto:'.$user_profile['field_business_email']['#items'][0]['value'].'">'.$user_profile['field_business_email']['#items'][0]['value'].'</a><br/>';  
  }?>
  <?php if(isset($user_profile['field_personal_email'])){
	print '<strong>Personal Email: </strong>';  
	print '<a href="mailto:'.$user_profile['field_personal_email']['#items'][0]['value'].'">'.$user_profile['field_personal_email']['#items'][0]['value'].'</a><br/>';  
  }?>
  </p>
  
  <!-- Class of -->
  <p style="margin-top:20px">
  <?php if(isset($user_profile['field_class_of'])){
	print '<strong>Class of: </strong>';  
	print $user_profile['field_class_of'][0]['#title'];  
  }?>
  </p>
  
</div>

