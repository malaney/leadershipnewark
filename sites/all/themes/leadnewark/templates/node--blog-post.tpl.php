<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

	
  	<div class="content"<?php print $content_attributes; ?>>
    
    <!-- Page titles for teasers only -->
    <?php if ($page==0): ?>
    	<?php print render($title_prefix); ?>  
    	<h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>">				
		<?php print $title; ?></a></h2>
  		<?php print render($title_suffix); ?>
	<?php endif; ?>
	  
    <?php nwk_edit(); ?> 
    
    <!-- Byline -->
    <?php nwk_byline($uid); ?>
      
	<?php    
      hide($content['comments']);
      hide($content['links']);
      print render($content);
	 
    ?>
  	</div>

  	<?php print render($content['links']); ?>

  	<?php print render($content['comments']); ?>

</div>
