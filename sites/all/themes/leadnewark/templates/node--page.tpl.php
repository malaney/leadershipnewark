<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  	<div class="content"<?php print $content_attributes; ?>>
    
        <div id="page-left"> 
            
            <!-- Page Images -->
            <div id="page-images">
                <?php print render($content['field_page_image']); ?>
            </div>
                
        </div><!-- End #page-left -->
                     
        <div id="page-right">
        	
            <!-- Heading Page Title -->
            <?php if (!$page): ?>
            <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
            <?php endif; ?>  
            
            <?php nwk_edit(); ?>
                           
            <?php 
				// hide stuff to display later
				hide($content['comments']);
				hide($content['links']);
				hide($content['field_page_image']);	
				print render($content); 
			?>
            
        </div><!-- End #page-right -->
    
  	</div><!-- End .content -->


</div>
