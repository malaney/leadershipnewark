<div id="body-wrap">

    <div id="wrapper">
    
		<?php include("sites/all/themes/leadnewark/inc/header.php"); ?>
    
        <div id="main">
        
        	
            <div id="content">
            	<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
                
                <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>
                
                 <?php print render($page['content']); ?>
        
            </div><!-- End #content -->
        
        </div><!-- End #main -->
        
        <?php include("sites/all/themes/leadnewark/inc/footer.php"); ?>
    
    </div><!-- End #wrapper -->
    
</div><!-- End #body-wrap -->