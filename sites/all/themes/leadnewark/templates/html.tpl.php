<!DOCTYPE HTML>
<html lang="en">

<head profile="<?php print $grddl_profile; ?>">
	<?php print $head; ?>
    <meta charset=utf-8>
    
    <!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
    
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
    <?php print $scripts; ?>
    
    <!-- Load slideshow scripts for home page -->
    <?php if($is_front){
		print '<script type="text/javascript" src="/sites/all/themes/leadnewark/js/slides.min.jquery.js"></script>';
		print '<script type="text/javascript" src="/sites/all/themes/leadnewark/js/slides.js"></script>';
	} ?>
    
    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600|Fanwood+Text:400,400italic' rel='stylesheet' type='text/css'>
</head>

<body class="<?php print $classes; ?> <?php bodyclass(); ?>" <?php print $attributes;?>>
	<?php print $page_top; ?>
    <?php print $page; ?>
    <?php print $page_bottom; ?>
</body>
</html>