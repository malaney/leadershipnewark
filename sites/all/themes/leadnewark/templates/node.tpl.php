<?php 
// if a pdf file attached, we need a class to rearrange the sharing buttons
$z = '';
if (isset($content['field_pdf_file'])) {
	$z = 'withpdf';
}
?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix <?php print $z; ?>"<?php print $attributes; ?>>

	
  	<div class="content"<?php print $content_attributes; ?>>
    
    <!-- Page titles for teasers only -->
    <?php if ($page==0): ?>
    	<?php print render($title_prefix); ?>  
    	<h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>">				
		<?php print $title; ?></a></h2>
  		<?php print render($title_suffix); ?>
	<?php endif; ?>
	  
    <?php nwk_edit(); ?> 
      
	<?php    
      hide($content['comments']);
      hide($content['links']);
	  hide($content['field_pdf_file']);
      print render($content);
	  
	  // pdf file
	  if ($z == 'withpdf') {
		print '<div class="pdf-download">';  
		print '<h3>Download the PDF</h3>'; 
		print render($content['field_pdf_file']);
		print '<p>Viewing these reports requires Adobe Reader be installed on your device.  If it\'s not currently installed, <a href="http://get.adobe.com/reader/" target="_blank">click here</a> to download.</p>';
		print '</div>';
	  }
    ?>
    
    <div class="myclear"></div>
    
  	</div>

  	<?php print render($content['links']); ?>

  	<?php print render($content['comments']); ?>
        
</div>
