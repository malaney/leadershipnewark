<?php 
// Remove 'no content' message
unset($page['content']['system_main']['default_message']);
?>

<style>
.slides_container {background-image:url(/<?php print random_pic('sites/all/themes/leadnewark/img/slides/bkgrnds'); ?>) !important}
</style>

<div id="body-wrap">

    <div id="wrapper">
    
        <?php include("sites/all/themes/leadnewark/inc/header.php"); ?>
    
        <div id="main">
        
        	<!-- Slideshow -->
            <div id="slides">
                <div class="slides_container">
                    <div><img src="/sites/all/themes/leadnewark/img/slides/slide1.png" alt="Leadership Newark"></div>
                    <div><img src="/sites/all/themes/leadnewark/img/slides/slide2.png" alt="Leadership Newark"></div>
                    <div><img src="/sites/all/themes/leadnewark/img/slides/slide3.png" alt="Leadership Newark"></div>
                    <div><img src="/sites/all/themes/leadnewark/img/slides/slide4.png" alt="Leadership Newark"></div>
                    <div><img src="/sites/all/themes/leadnewark/img/slides/slide5.png" alt="Leadership Newark"></div>
                </div>
            </div>
            
        
            <div id="content">
            	
               <?php print render($page['content']); ?>
                       
            </div><!-- End #content -->
        
        
            <!-- Begin Right Column -->        
            <div id="rightcol"> 
                <?php print render($page['right_side']); ?>
            </div>
            <!-- End Right Column -->
        	
            <div class="myclear"></div>
        
        </div><!-- End #main -->
        
        <?php include("sites/all/themes/leadnewark/inc/footer.php"); ?>
    
    </div><!-- End #wrapper -->
    
</div><!-- End #body-wrap -->