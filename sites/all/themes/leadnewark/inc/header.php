<header class="hd1">

	<a href="/"><div id="logo">Leadership Newark: Working Together For Newark's Future</div></a>

    <nav>
    <?php print render($page['navbar']); ?>
    </nav>
    
    <div id="headerMenu">
    
    <?php if($logged_in) {    
       print theme('links', array('links' => menu_navigation_links('menu-logged-in-header'), 'attributes' => array('class'=> array('links', 'user-menu')) ));}
	   else {
	   	print theme('links', array('links' => menu_navigation_links('menu-logged-out-header'), 'attributes' => array('class'=> array('links', 'user-menu')) ));}
	   ?>
    </div>

</header>