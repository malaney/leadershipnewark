<footer class="ft1 myclear">

    <div id="socialmedia">
    	<span>Join Leadership Newark on</span> 
		<a href="https://twitter.com/LeadershipNwk" class="twitter" target="_blank">Twitter</a>
        <a href="https://www.facebook.com/pages/Leadership-Newark/121759421205578" class="facebook" target="_blank">Facebook</a>
        <a href="http://www.linkedin.com/pub/leadership-newark/7/579/46b" class="linkedin" target="_blank">LinkedIn</a>
        <?php if($logged_in) { ?>
        <div id="edit-profile-footer"><?php print l(t('Edit Profile'), "user/{$GLOBALS['user']->uid}/edit"); ?></div>
       <?php  } ?>
    </div>
    
    <div id="copyright">&copy;<?php print date("Y"); ?> Leadership Newark - All Rights Reserved. Site by <a href="http://www.inspirawebdesign.com" target="_blank">Inspira Web Design.</a></div>

</footer>